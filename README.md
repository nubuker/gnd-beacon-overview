# GND BEACON Overview

Zahlreiche Online-Angebote annotieren ihre Daten und Metadaten anhand der Gemeinsamen Normdatei (GND) und richten Schnittstellen ein, anhand derer sie sich gezielt durch Übergabe von GND-Identifiern abfragen lassen. 

Die Findbarkeit solcher durch GND-IDs charakterisierter Ressourcen wird durch die Veröffentlichung sogenannter BEACON-Dateien gefördert.

Die interaktive, d.h. klickbare [Visualisierung "GND BEACON Overview"](https://nubuker.gitlab.io/gnd-beacon-overview/) dient der Erkundung und Veranschaulichung der GND-BEACON-Landschaft und ist im Zusammenhang der Mitarbeit des Steinheim-Instituts in der Task Area Editionen des NFDI-Konsortiums Text+ entstanden. Motivation ist hierbei insbesondere die Frage, ob und wie sich diese bottom-up gewachsene GND-BEACON-Landschaft in eine ForschungsDatenperspektive integrieren kann. Siehe dazu auch den Blogbeitrag: "[Digitale Editionen und die vernetzte GND-BEACON-Landschaft](https://textplus.hypotheses.org/752)". 

Die klickbaren Elemente führen zu einzelnen Angeboten bzw. demonstrieren anhand einer bestimmten GND-ID die Funktion dieser Angebote (oft "Ludwig Philippson", dessen ID auch im [STI Linked Data Service](http://www.steinheim-institut.de/see-also/query.html?text1=116176288&kategorie1=GND) als Beispiel dient).

Die Quelldatei liegt (zur Zeit) im Graphviz-Format vor. Die Web-Visualisierung importiert eine gerenderte und (zur Zeit) manuell nachbearbeitet SVG-Fassung.


Die Visualisierung ist unvollständig, inhaltlich und technisch in einem sehr frühen Stadium -- Version 0.01α -- und Work in Progress. Eine Weiterentwicklung auf Basis der "browser based visualization library" vis.js https://visjs.org/ ist in Vorbereitung.

Lizenz: CC-BY-SA-4.0

## Mehr zum Thema GND-BEACON
-  Lordick, Harald;  Mache, Beata: Annotationen anhand der Gemeinsamen Normdatei aus einer anwendungsorientierten Perspektive historischer Forschung. Digital Humanities im deutschsprachigen Raum (DHd2018), Köln, 26.02.2018-02.03.2018 https://doi.org/10.5281/zenodo.1188229
    -  Abstract: https://doi.org/10.5281/zenodo.4622478
- Workshop auf der DHd 2024: Al-Eryani, S., Gerstner, E.-M., Hegel, P., Lordick, H., Schnöpf, M., Schulz, D., & Sikora, U. (2024, Februar 21). BEACONe unde venis et quo vadis?. DHd 2024 Quo Vadis DH (DHd2024), Passau, Deutschland. (Abstract https://zenodo.org/doi/10.5281/zenodo.10698320 sowie Folien https://zenodo.org/doi/10.5281/zenodo.10732654 )
- Beiträge zu GND-BEACON auf der Hypotheses-Plattform: https://djgd.hypotheses.org/tag/beacon    

...        
